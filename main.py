import speech_recognition as sr
import pyttsx3
import datetime

import os
import time
import subprocess

import json
import requests
import sounddevice as sd
import pyjokes

engine=pyttsx3.init('sapi5')
voices=engine.getProperty('voices')
engine.setProperty('voice','voices[1].id')

mic_index = 2 #this is the device index. Probabl will differ per computer,so has to be checked first by doing sr.Microphone.list_microphone_names()
def speak(text):
    engine.say(text)
    engine.runAndWait()
    
def wishMe():
    hour=datetime.datetime.now().hour
    if hour>=0 and hour<12:
        speak("Good Morning")
        print("Good Morning")
    elif hour>=12 and hour<18:
        speak("Good Afternoon")
        print("Good Afternoon")
    else:
        speak("Good evening")
        print("Good evening")

def takeCommand():
    r=sr.Recognizer()
    with sr.Microphone(device_index=mic_index) as source:
        print("Talk now...")
        r.adjust_for_ambient_noise(source, duration = 1)
        audio=r.listen(source)
        

        try:
            statement=r.recognize_google(audio,language='en')#username = user , password = pwd, language='en')
            print(f"user said:{statement}\n")

        except Exception as e:
            print(e)
            speak("Say what?")
            return "None"
        return statement
    
 print("Getting ready")
#speak("Getting ready")
wishMe()

if __name__=='__main__':


    while True:
        speak("How can I help you?")
        statement = takeCommand().lower()
        if statement==0:
            continue
        if "good bye" in statement or "bye" in statement or "stop" in statement:
                speak('very well, good bye')
                print('very well, good bye')
                break
        elif "ben is awesome" in statement:
            speak ('its true')
            print ('yes you did it!')
            
        elif "joke" in statement:
            joke = str(pyjokes.get_joke())
            speak (joke)
            print ('yes you did it!')
       # elif "is it going to rain?"

        elif "log off" in statement or "sign out" in statement:
                speak("Ok , your pc will log off in 10 sec make sure you exit from all applications")
                subprocess.call(["shutdown", "/l"])
    time.sleep(3)