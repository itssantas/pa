recipes = {'scones':{'ingredients':"""225 gram self raising flour, pinch of salt, 55 gram  butter, 25 gram caster sugar, 150 ml milk plus to glaze""" ,
                    'method': 
                     """
                   1.  Heat the oven to 220 or 200 Fan and lightly grease a baking tray.

                   2. Mix together the flour and salt and rub in the butter. Stir in the sugar and then the milk to get a soft dough.

                   3. Turn on to a floured work surface and knead very lightly. Pat out to a round 2cm/¾in thick. Use a 5cm/2in cutter to stamp out rounds and place on the baking tray. Lightly knead together the rest of the dough and stamp out more scones to use it all up.

                   4. Brush the tops of the scones with the beaten egg. Bake for 12-15 minutes, or until well risen and golden-brown.

                   5. Cool on a wire rack and serve with butter and good jam and maybe some clotted cream. 
                   """
                    }
          }

                   